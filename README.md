# Dice Castle

Un héros entre dans un chateau à la recherche de l'épée magique.
Il va devoir affronter des monstres et des pièges.

## Fonctionnalités

### Itération 1 :
 * Le héros démarre la partie à l'extérieur du chateau.
 * A chaque tour, le héros avance d'une case.
  * Le chateau est constitué d'un enchainement de 17 cases.
 * Les cases de 1 à 16 sont vides.
 * A la fin de son déplacement, le joueur sait sur quelle case se trouve le héros.
 * la case 17 contient l'épée magique, si le héros arrive sur cette case, il obtient l'épée.
 * La partie se termine lorsque le héros obtient l'épée magique.

### Itération 2 : 
 * Le héros peut se déplacer dans le chateau en lançant un d6.
 * Lorsqu'il arrive ou dépasse la case 17, il obtient l'épée magique.

### Itération 3 :
 * Le héros commence l'aventure avec 3 points de vie.
 * Le héros a un maximum de 5 points de vie.
 * Le héros peut perdre des points de vie lorsqu'il perd un combat contre un monstre.
 * Si le héros à moins de 1 point de vie, il est mort, la partie est terminée.

### Itération 4 :
 * Les cases '2' et '5' contiennent des monstres de niveau 1.
 * La case '15' contient un monstre de niveau 2.
 * La case '16' contient un monstre de niveau 3 (le Roi Squelette).
 * La case "13' contient 2 potions de soin.
 * Une potion de soin permet d'ajouter un point de vie.

#### Résolution des cases monstres :
 * un monstre de niveau 1 => lancer un d6 :
    * si le résultat est 1 ou 2 : le héros perd 1 point de vie.
    * si le résultat est 3, 4, 5, 6 : le héros ne perd pas de point de vie.
 * un monstre de niveau 2 => lancer un d6 :
    * si le résultat est 1, 2, 3 : le héros perd 1 point de vie.
    * si le résultat est 4, 5, 6 : le héros ne perd pas de point de vie.
  * un monstre de niveau 3 => lancer un d6 :
      * si le résultat est 1, 2, 3 : le héros perd 2 points de vie.
      * si le résultat est 4, 5, 6 : le héros ne perd pas de point de vie.


### Lexique :
 * d6 => dé à 6 faces.
